class UsersController < ApplicationController

  def show
    load_user
  end

  def edit
    load_user
  end

  def load_user
    @user = User.find params[:id]
    # if @user = "sign_out"
    #   sign_out current_user
    #   return
    # end
  end

  def user_params
    params[:user].permit(:prenom,:nom,:email,:password,:password_confirmation)
  end

  def update
    @user = User.find(params[:id])
    # if params[:user][:password].empty? && params[:user][:password_confirmation].empty?
    #   params[:user].delete :password
    #   params[:user].delete :password_confirmation
    # end
    if @user.update_attributes(user_params)
        redirect_to action: 'show'
    else
         redirect_to action: 'edit'
    end
  end

  def new
    @user = User.new
    render 'new'
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to :articles
    else
      render :new
    end
  end

  def destroy
    @user = User.find(params[:id])
    # @article = Article.where(["user_id = ?", @user])
    # @article.delete(self)
    @user.destroy
    redirect_to [:articles]
    - if @user.destroy
      flash[:success] = "Your account has been deleted."
    end

    # redirect_to [:articles]
  end
end
