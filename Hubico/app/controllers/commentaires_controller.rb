class CommentairesController < ApplicationController
  def create
    @commentaire = Commentaire.new(params[:commentaire].permit(:contenu, :user_id, :article_id, :date_commentaire))
    @article = Article.find(params[:article_id])

    @commentaire.user = current_user
    @commentaire.article = @article
    @commentaire.date_commentaire = Time.new

    @commentaire.save
    redirect_to article_path(@article)
  end

  def destroy
    @commentaire = Commentaire.find(params[:id]).destroy
    redirect_to [:articles]
  end
end
