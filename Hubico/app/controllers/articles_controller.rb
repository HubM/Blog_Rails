class ArticlesController < ApplicationController

  def show
    load_article
  end

  def edit
    load_article
  end

  def load_article
    @article = Article.find params[:id]
  end

  def article_params
    params[:article].permit(:titre, :date, :entete, :contenu, :image, :user_id)
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(article_params)
      flash[:notice] = "Successfully updated article!"
      flash.notice = "article successfully updated"
      redirect_to action: 'show'
    else
         redirect_to action: 'edit'
    end
  end

  def new
    @article = Article.new
    render 'new'
  end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    @article.date = Time.new
    if @article.save
      redirect_to [:articles]
    else
      render :new
    end
  end

    def destroy
      @article = Article.find(params[:id]).destroy!
      redirect_to [:articles]
    end
end
