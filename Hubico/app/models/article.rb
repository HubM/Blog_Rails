class Article < ApplicationRecord
  belongs_to :user
  has_many :commentaires, :dependent => :destroy

  mount_uploader :image, ImageForArticleUploader
end
