Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'users' }

  # match '/users/:id', :to => 'users#show', :as => :user
  resources :articles, path: '/'
  resources :users
  resources :articles do
    resources :commentaires
  end
  # resources :user
  # match '/user/:id/edit'
  # resources :edit
  # resources :create
  # resources :destroy
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
