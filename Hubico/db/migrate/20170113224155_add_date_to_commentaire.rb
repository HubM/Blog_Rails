class AddDateToCommentaire < ActiveRecord::Migration[5.0]
  def change
    add_column :commentaires, :date_commentaire, :date
  end
end
