class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :titre
      t.date :date
      t.text :entete
      t.text :contenu
      t.timestamps null: false
    end
  end
end
